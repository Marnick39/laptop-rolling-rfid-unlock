/**
 * ----------------------------------------------------------------------------
 * This is a MFRC522 library example; see https://github.com/miguelbalboa/rfid
 * for further details and other examples.
 *
 * NOTE: The library file MFRC522.h has a lot of useful info. Please read it.
 *
 * Released into the public domain.
 * ----------------------------------------------------------------------------
 * This sample shows how to read and write data blocks on a MIFARE Classic PICC
 * (= card/tag).
 *
 * BEWARE: Data will be written to the PICC, in sector #1 (blocks #4 to #7).
 *
 *
 * Typical pin layout used:
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
 *
 */

#include <SPI.h>
#include <MFRC522.h>
#include <KeyboardAzertyFr.h>
#include <EEPROM.h>

#define RST_PIN         9
#define SS_PIN          10

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

MFRC522::MIFARE_Key key;
// Previously written data (should me stored in EEPROM)
const unsigned int keySize = 62; // Size of randomized key stored in the card, in bytes
const unsigned int crcSize = 1; // Size of the CRC data appended to the end of the key when stored in EEPROM, in bytes
//const unsigned int dataSize = keySize + crcSize; // Size of all the data together when stored in EEPROM TODO implement CRC decently
const unsigned int dataSize = keySize; // Size of all the data together when stored in EEPROM
const unsigned int badAuthenticationTimeout = 1; // Amount of seconds to be waited after a failed authentication

// Designated whether this is the initilializing run (stored in EEPROM)
bool initializing = true;

// Timing vars
unsigned long t1, t2, res, sessionStart;

// EEPROM addresses in bytes (512 bytes of data)
unsigned int eeinitializing = 0; // Where the 'initializing' variable is stored.
unsigned int eeKey = 1; // Where the key + its crc is stored.
unsigned int eeUID = dataSize + 2; // Where the UID is stored.

byte prevDataBlock [dataSize];
byte prevUID [16];
bool goodSetup = false;
MFRC522::StatusCode status;

/**
 * Initialize.
 */
void setup() {      
    Serial.begin(9600); // Initialize serial communications with the PC
    randomSeed(analogRead(A1)); // Initialize the pseudorandom source with random-ish seed
    KeyboardAzertyFr.begin();
    while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
    SPI.begin();        // Init SPI bus
    mfrc522.PCD_Init(); // Init MFRC522 card

    // Prepare the key (used both as key A and as key B)
    // using FFFFFFFFFFFF which is the default at chip delivery from the factory
    for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
    }
    
    // Read backup data from EEPROM
    EEPROM.get(eeinitializing,initializing);
    //Serial.println(initializing);
    initializing =  true;
    goodSetup = true;
    for (byte i = 0; i < dataSize; i++)
    {
      EEPROM.get(eeKey + i,prevDataBlock[i]); 
    }

    for (byte i = 0; i < 16; i++){
      EEPROM.get(eeUID + i,prevUID[i]);
    }

    // Check validity of EEPROM memory, assuming crcSize = 1
/*    if(prevDataBlock[dataSize-1] != CRC8(prevDataBlock,dataSize-1)){ // TODO: implement CRC decently
      Serial.println("!!!EEPROM Memory error: CRC mismatch, aborting!!!");
      goodSetup = false; // Just to make sure..
    }else
      goodSetup = true;
 */    
    Serial.println(F("Scan a MIFARE Classic PICC to demonstrate read and write."));
    Serial.print(F("Using key (for A and B):"));
    dump_byte_array(key.keyByte, MFRC522::MF_KEY_SIZE);Serial.println("");

}

/**
 * Main loop.
 */
void loop() {
    // If the setup was bad and this isn't the first run, don't go on.
    if (!goodSetup && !initializing)
      return;
      
    // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
    if ( ! mfrc522.PICC_IsNewCardPresent())
        return;

    // Select one of the cards
    if ( ! mfrc522.PICC_ReadCardSerial())
        return;
        
    // Store current session starting time
    sessionStart = millis();
    // Show some details of the PICC (that is: the tag/card)
    Serial.print(F("Card UID:"));
    dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.println();
    Serial.print(F("PICC type: "));
    MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    Serial.println(mfrc522.PICC_GetTypeName(piccType));

    // Check for compatibility
    if (    piccType != MFRC522::PICC_TYPE_MIFARE_MINI
        &&  piccType != MFRC522::PICC_TYPE_MIFARE_1K
        &&  piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
        Serial.println(F("This sample only works with MIFARE Classic cards."));
        return;
    }

    ////////////////////////////////
    ///// CHECKING PREVIOUS UID ////
    ////////////////////////////////
    status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 3, &key, &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("PCD_Authenticate() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        return;
    }

    byte currentUID[18];
    byte len = 18;
    status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(0, currentUID, &len);
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("PCD_Read() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        return;
      }
      
    // Comparing Uniuqe ID..
    bool equalUID = true;
    for (byte i = 0; i < 16 && equalUID; i++) {
        // Compare buffer (= what we've just read) with dataBlock (= what we've written)
        if (prevUID[i] != currentUID[i]) // Might leak amount of incorrect bytes of Unique ID
            equalUID = false;
        //Serial.print("Comparing ");Serial.print(i);Serial.print(": "); Serial.print(prevUID[i]); Serial.print("| ");  Serial.println(currentUID[i]);
    }
    if (equalUID or initializing)
      Serial.println("UID accepted!");
    else{
      Serial.println("UID invalid, aborting!");
      return; 
    }

    if(initializing){
      // Since this is the first run, store the current UID in EEPROM for later authentication
      for (byte i = 0; i < 16; i++)
      {
          EEPROM.put(eeUID + i,currentUID[i]);
      }

      
      
    }
      
    
    ////////////////////////////////
    ///// CHECKING PREVIOUS KEY ////
    ////////////////////////////////


    byte sector         = 1;
    byte dataBlock[dataSize];

    byte trailerBlock   = 7;
    //Need to supply 2 extra bytes for reading
    byte buffer[dataSize + 2];
    byte size = sizeof(buffer)-2;
    

    
    // Reading previous key
    Serial.print(F("Reading data ... "));
    t1 = millis();
    readBlocks(buffer, keySize);
    t2 = millis();
    
    //Serial.print(F("Previous Key: "));
    //dump_byte_array(prevDataBlock, keySize); Serial.println();
    res = t2 - t1;
    Serial.print("Read completed in ");Serial.print(res);Serial.println("ms.");
    Serial.println();

    // Comparing..
    bool equal = true;
    for (byte i = 0; i < dataSize && equal; i++) {
        // Compare buffer (= what we've read) with dataBlock (= what we've written)
        if (prevDataBlock[i] != buffer[i])
            equal = false;
        //Serial.print("Comparing ");Serial.print(i);Serial.print(": "); Serial.print(prevDataBlock[i]); Serial.print("| ");  Serial.println(buffer[i]);

    }
    
    if (equal|| initializing){
    // Equal or initializing, the card is validated.
    
    if(initializing)
      Serial.println("Initial run detected!");
    else
      Serial.println("Key match!");
    
    

    
    //////////////////////////////////////////
    //////////////  WRITING  /////////////////
    //////////////////////////////////////////
    
    // Write data 
    Serial.println(F("Writing data.. "));
    
    // Generate random values for dataBlock
    for (byte i = 0; i < keySize; i++) {
        dataBlock[i] = random(255);// Off by one?
        //Serial.print("Writing data "); Serial.print(i);Serial.print(": "); Serial.println(dataBlock[i]);

    }
    t1 = millis();
    writeBlocks(dataBlock, dataSize);
    t2 = millis();
    res = t2 - t1;
    Serial.print("Write completed in ");Serial.print(res);Serial.println("ms.");
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Write() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
    }
    Serial.println();

    // Read data from the block (again, should now be what we have written)
    //Serial.print(F("Reading data from block ")); Serial.print(blockAddr);
    //Serial.println(F(" ..."));
    t1 = millis();
    readBlocks(buffer, keySize);
    if (status != MFRC522::STATUS_OK) {
       Serial.print(F("MIFARE_Read() failed: "));
       Serial.println(mfrc522.GetStatusCodeName(status));
    }
    
    // Check that data in block is what we have written
    // by counting the number of bytes that are equal
    Serial.print(F("Verifying result... "));
    byte equal = true;
    for (byte i = 0; i < keySize && equal; i++) {
        // Compare buffer (= what we've read) with dataBlock (= what we've written)
        if (buffer[i] != dataBlock[i])
            equal = false;
    }
    if (equal) {
      // New data is written correctly
      t2 = millis();
      res = t2 - t1;
      Serial.print(F( "Write verified in "));Serial.print(res);Serial.println("ms!");

      // Send password to computer
      entedPasswordPseudoSecurely("Password");
      
      Serial.print("Authenticated after ");Serial.print(millis() - sessionStart);Serial.println("ms of validation.");

      // Calculate CRC for the new key TODO: Implement CRC decently
      //dataBlock[dataSize-1] = CRC8(dataBlock,dataSize-1);
     
      // Backup written Key + calculated CRC
      for (byte i = 0; i < dataSize; i++)
      {
          prevDataBlock[i] = dataBlock[i];
          EEPROM.put(eeKey + i,dataBlock[i]);
      }

      
        
    } else {
        Serial.println(F("Write failure, no match!"));
    }



}
 else {
 // Not Equal!
      Serial.println("Keys do not match!");
      Serial.println("Previous key: ");
      dump_byte_array(prevDataBlock, dataSize); Serial.println();
      Serial.println("Current key: ");
      dump_byte_array(buffer, dataSize); Serial.println();
      delay(badAuthenticationTimeout*1000);
    }


    // Ending Sequence..
    // End any initializing run
    initializing = false; 
    // Halt PICC
    mfrc522.PICC_HaltA();
    // Stop encryption on PCD
    mfrc522.PCD_StopCrypto1();

    Serial.print("Total runtime: ");Serial.print(millis() - sessionStart);Serial.println("ms."); 
    Serial.println("///////////////////////////////////////////////////////////");
    
}

/**
 * Helper routine to dump a byte array as hex values to Serial.
 */
void dump_byte_array(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
      Serial.print(buffer[i] < 0x10 ? " 0" : " ");
      Serial.print(buffer[i], HEX);
  }
}

/**
 * Helper routine to write a byte array into multiple blocks of memory.
 */
void writeBlocks(byte *data, byte dataSize){
  // Introduce a decrementing counter that tracks how much data needs to be written still, expressed in bytes
  int dataLeft = dataSize;
  // Calculate the required amount of sectors to write to get all of the data (3 blocks/sector & 16 bytes/block)
  int sectors = dataSize / (3*16);
  if (dataSize % (3*16) > 0) sectors++;
  //Serial.print("Will be writing ");Serial.print(sectors);Serial.println(" sector(s).");
  // Prepare a status for the writing process.
  
  MFRC522::StatusCode status;
  // Loop over the required sectors, every sector contains 4 blocks, amount of sectors is 16 in the classic case
  // but we're ignoring the 1st sector since it contains the UID in block 0
  byte *dataAddr = data;
  byte len = 16;
  for (byte sect = 1; sect < 16 and sect <= sectors; sect++) {
    // Loop over the required blocks, every block contains 16 bytes, amount of blocks per sector is 4 in the classic case
    // but we're ignoring the last block since it contains the access-control data and the keys for the sector
    for (byte blck = 0; blck < 3 and 0 < dataLeft; blck++) {
      // Authenticate for this sector using the generic A key
      int trailerBlock = (sect * 4) + 3; // Signifies the last block of this sector, where the keys etc are stored
      status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
      if (status != MFRC522::STATUS_OK) {
        Serial.print(F("PCD_Authenticate() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        return;
      }
      // Calculate the block address
      int blockAddr = sect * 4 + blck;
      //Serial.print("Currently writing block ");Serial.print(blockAddr);Serial.print(" in sector ");Serial.println(sect);
      // Every block can store 16 bytes of data, check how many bytes are required first
      if (dataLeft >= 16) {
        //Serial.println("Writing 16 of the remaining bytes.");
        //Serial.print("Storing these bytes with offset ");Serial.println(dataAddr - data);
        // Write the full 16 bytes of this block
        status = (MFRC522::StatusCode) mfrc522.MIFARE_Write(blockAddr, dataAddr,len);
        // decrement the amount of dataLeft by 16 bytes
         dataLeft -= 16;
      }
      else {
        //Serial.print("writing remaining ");Serial.print(dataLeft);Serial.println(" bytes.");
        //Serial.print("Storing these bytes with offset ");Serial.println(dataAddr - data);
        // Write the remaining bytes, the lib needs 16 byte chunks, so get it a new array of 16 bytes..
        byte bufferedData [16];
        //Serial.println("Copying data into new array.. ");
        
        for (byte i = 0; i < dataLeft; i++) {
          bufferedData[i] = dataAddr[i];
        }
        status = (MFRC522::StatusCode) mfrc522.MIFARE_Write(blockAddr, bufferedData,len); 
        // No data left to write
        dataLeft = 0; 
      }
      // Check if write was succesful
      if (status != MFRC522::STATUS_OK) {
          Serial.print(F("MIFARE_Write() failed: "));
          Serial.println(mfrc522.GetStatusCodeName(status));
          return;
        }
      // Increment the data array pointer, needs to move along as we write data
      dataAddr += 16;
    }
  }
} 


/*
 * Attempts to make sure that the password is only typed in when on tty1 and in the lock screen, to prevent password leakes.
 * !!!! Easily defeated !!!!
 */

void entedPasswordPseudoSecurely(String password){
  //----------- Make sure tty1 is active
    KeyboardAzertyFr.press(KEY_LEFT_CTRL);
    KeyboardAzertyFr.press(KEY_LEFT_ALT);
    KeyboardAzertyFr.press(KEY_F1);
    KeyboardAzertyFr.releaseAll();
    // Wait for ctrl + alt + f1 to switch to tty1
    delay(1000);
    
    //----------- Make sure computer is locked
    KeyboardAzertyFr.press(KEY_LEFT_GUI);
    KeyboardAzertyFr.press('l');
    KeyboardAzertyFr.releaseAll();
    // Remove potential 'l' in password box, attempting to lock when already locked will leave an 'l' behind.
    KeyboardAzertyFr.press(KEY_BACKSPACE)
    KeyboardAzertyFr.releaseAll();
    // Wait for locking mechanism to kick in
    delay(2000);
    
    //----------- Type in password
    KeyboardAzertyFr.print(password);  
    // Press 'enter' (carriage return) 
    KeyboardAzertyFr.print(KEY_RETURN);
}

/**
 * Helper routine to read data out of multiple blocks of memory.
 */
void readBlocks(byte *data, byte dataSize){
// Introduce a decrementing counter that tracks how much data needs to be written still, expressed in bytes
  int dataLeft = dataSize;
  // Calculate the required amount of sectors to read to get all of the data (3 blocks/sector & 16 bytes/block)
  int sectors = dataSize / (3*16);
  if (dataSize % (3*16) > 0) sectors++;
  //Serial.print("Will be reading ");Serial.print(sectors);Serial.println(" sector(s).");
  // Prepare a status for the reading process.
  MFRC522::StatusCode status;
  // Loop over the required sectors, every sector contains 4 blocks, amount of sectors is 16 in the classic case
  // but we're ignoring the 1st sector since it contains the UID in block 0
  byte *dataAddr = data;
  for (byte sect = 1; sect < 16 and sect <= sectors; sect++) {
    // Loop over the required blocks, every block contains 16 bytes, amount of blocks per sector is 4 in the classic case
    // but we're ignoring the last block since it contains the access-control data and the keys for the sector
    for (byte blck = 0; blck < 3 and 0 < dataLeft; blck++) {
      // Authenticate for this sector using the generic A key
      int trailerBlock = (sect * 4) + 3; // Signifies the last block of this sector, where the keys etc are stored
      status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
      if (status != MFRC522::STATUS_OK) {
        Serial.print(F("PCD_Authenticate() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        return;
      }
      // Calculate the block address
      int blockAddr = sect * 4 + blck;
      //Serial.print("Currently reading block ");Serial.print(blockAddr);Serial.print(" in sector ");Serial.println(sect);
      // Every block can store 16 bytes of data, check how many bytes are required first
      if (dataLeft >= 16) {
        //Serial.println("Reading 16 of the remaining bytes.");
        //Serial.print("Reading these bytes with offset ");Serial.println(dataAddr - data);
        // Read the full 16 bytes of this block
        byte len = 18;
        status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(blockAddr, dataAddr,&len);
        // decrement the amount of dataLeft by 16 bytes
         dataLeft -= 16;
      }
      else {
        //Serial.print("reading remaining ");Serial.print(dataLeft);Serial.println(" bytes.");
        //Serial.print("Storing these bytes with offset ");Serial.println(dataAddr - data);
        // Read the remaining bytes, the lib needs 18 byte block reads, so get it a new array of 18 bytes..
        byte bufferedData [18];
        byte len = 18;
        status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(blockAddr, bufferedData, &len); 
        //Serial.println("Copying data into new array.. ");
        for (byte i = 0; i < dataLeft; i++) {
          dataAddr[i] = bufferedData[i];
        }
        // No data left to write
        dataLeft = 0; 
      }
      // Check if read was succesful
      if (status != MFRC522::STATUS_OK) {
          Serial.print(F("MIFARE_Read() failed: "));
          Serial.println(mfrc522.GetStatusCodeName(status));
          return;
        }
      // Increment the data array pointer, needs to move along as we write data
      dataAddr += 16;
    }
  }
}


/** 
 * Helper routine to calculate the 8 bit CRC of a byte array.
 * 
 * CRC-8 - based on the CRC8 formulas by Dallas/Maxim
 * code released under the therms of the GNU GPL 3.0 license
 * https://www.leonardomiliani.com/en/2013/un-semplice-crc8-per-arduino/
*/
byte CRC8(const byte *data, byte len) {
  byte crc = 0x00;
  while (len--) {
    byte extract = *data++;
    for (byte tempI = 8; tempI; tempI--) {
      byte sum = (crc ^ extract) & 0x01;
      crc >>= 1;
      if (sum) {
        crc ^= 0x8C;
      }
      extract >>= 1;
    }
  }
  return crc;
}
