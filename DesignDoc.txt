Goal: Unlock desktop

Why: 

How:
  - Overview: Arduino emulates a keyboard and writes a password when the correct data was read from the correct rfid tag (check ID & data), every read will also trigger a write to the data to change access tokens every time.


  - Details:
      - Read data block 0 (ID), check with required ID field
      - Read 


Extra:
  - Use timing to allow different password read-outs
  - Use a button to communicate what password you want to arduino to print
  - Use an LED to signify what status the arduino is in - could be used to visibly switch between passwords
  - Execute additional modifcations to laptop (improved ventilation around cpu fan, dust filters, 
  - Write data all across the card, making it unclear (using rolling key locations) where the token 

Links:
  https://create.arduino.cc/projecthub/najad/lock-unlock-your-computer-using-rfid-24cd7f
  https://www.arduino.cc/reference/en/language/functions/usb/keyboard/keyboardwrite/
  https://www.youtube.com/watch?v=2jY5_yTBM70
  https://www.arduinolibraries.info/libraries/keyboard-azerty-fr
  https://learn.adafruit.com/adafruit-pn532-rfid-nfc?view=all

Keys:
  - Pseudorandomly generated key with unconnected analog.read seed (real rng is hard, but vast improvements are probably possible)
  - Keysize: 496 bits / 62 bytes

EEPROM storage: 512 bytes
  - Allocation table:
         0 | 1  ..  62  63  | 64 .. 125  126 | 127     || 128 .. 255 || 256 .. 383 || 384 .. 511    (byte address)
   [WRITES]| [--KEY--][CRC] | [--KEY--][CRC] | [POS] || [-PATCH 2-]|| [-PATCH 3-]|| [-PATCH 4-]   (designation)

  - (todo) store key twice to protect against memory anomalies (cosmic rays are not your friend!)
  - (todo) use first 16 bytes of key to store UID of the authorized card
  - (todo) store an 8 bit crc for each key
  - (todo) cycle the used memory patch around to spread the wear evenly (using WRITES as a counter)
  - (todo) use a 1 byte incremental counter (256 values) as a trigger to move to the next patch of memory
  - Life estimation: (62 byte key + 1 byte crc) * 2 + 1 byte incremental counter + 1 spare byte
                      = 126B + 2B = 128 bytes total => 512 bytes of EEPROM / 128 bytes total = 4 memory patches
                      => EEPROM writability = 100.000 / 365 (days) / 12 (hours) = 22.83 available writes per hour
                      = ~22 uses per hour during the day for 1 year per memory patch * 4 memory patches
                      => 4 years of continual use before EEPROM memory slowly begins to deteriorate
                      (damage expresses itself as a reduction in write durability, eventually writing will stop)
  -> Use that spare byte for tracking on which memory patch we're currently working? (seems legit!)

Benchmarking:
-- Read/write 32 bytes ~ 0.026 + 0.028 = 0.054 sec (read + validation + keygen + writing)
-- Read/write 64 bytes ~ 0.108 sec
-- Safe Read/write 64 bytes ~ 0.052 + 0.056 + 0.052 = 0.160 sec (read + validation + keygen + writing + read check)

-- Reading: (experimental data | unoptimized: printing)
  Average: 1024 bytes / 833 ms = 1229 bytes / sec 
  Fastest: 832 | Slowest: 834

-- Writing: (experimental data | optimized-ish)
  Average: 16 bytes / 14 ms = 1142 bytes / sec 
  Fastest: 13 | Slowest: 15

-- Advertised:
  2.5 ms read block 
  6.0 ms write block
  2.5 ms dec/increment
  4.5 ms transfer (to/from built-in EEPROM)

TODO:
- Test rudimentary CRC implementation!!
- The output of analogRead() seems to be very localized, and thus unfit as a seed for the PRNG? Fix it?
- Find out if authentication is required per sector (when keys are all the same) or if it's bulk-able
- Add CRC checks on EEPROM upon booting (use a duplication scheme and crc every partition separately | 512 bytes)
- Add UID check (first 16 bytes of key)
- Add full-reset key (to reset device to uninitialized, to start a new cycle, for when something went wrong) 
- Use a low power mode when laptop is asleep? (http://www.gammon.com.au/power)
    -> userspace application sending serial data when sleeping/waking up + SerialEvent as trigger
- Reset pseudorandom generator after random amounts of time? (not worth cause pseudo is bad anyway?)
- Get an RFID card to restart arduino (EEPROM! Difficult to do, only soft-restart seems to be possible..)
- How to do multi password support? (sound & rythm, duration, button/LED in webcam hole, thin plastic control through dvd cover slids to switch internal button, use magnet,sendor and use finger position to change field)
- Hide the weirdness of the mfrc522 library parameters for the user of helper functions. (rip performance?)


mfrc522 library weirdness:
  - Upon reading/writing there's an 18 byte buffer required, even when reading/writing 16 bytes.
  - Make sure to pass the address (&) of the length parameter.
  - When passed too little space (no 2 extra) or a length smaller than 16, it fails.



IMPROVEMENTS
  - Either store Key in the form of a hash (to reduce required storage), no fully copies or better yet:
  - Encrypt list of passwords with code generated when using and stored in EEPROM (512 bytes/2 ~ max 13 passwords)
    -> Use preknown 'preamble' or hash to make sure decryption went correctly
  - Use Serial input and make a small application to tell the arduino which password it should print
  - (?) Use the TRNG you made inside of the arduino to create passwords and generate keys (?)



To process:
- FIPS 140-* rating for hardware security tips
- Store not only the passprase for the password manager, but also the keys for full disk encryption (w/o pass)
- Plant master password on safe, encrypted, medium (vault) to guarantee availability
- 'security bit' on arduino -> Makes memory unreadable (electronically) but doesn't mediate any attacker model..?
- Warning: Because CRC is linear an adversary can use linear algebra to tamper with the key w/o being noticed.
- To implement: key in hand gebruiken om eeprom key memory te encrypten in arduino memory (gebruik de crc om te zien of decryptie gelukt is) dit zorgt ervoor dat een gestolen laptop 
- backup keyfob to override primary keys (-> introduce gradual ease of use levels with solid security/reliability factor)

